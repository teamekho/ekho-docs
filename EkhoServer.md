# Ekho Server

Ekho's Java server facilitates XMPP and Websocket connections between device and client.    


## Requirements and Dependencies

* JDK 1.8
* Gradle 2.2.0
* Smack (XMPP library) 4.1.9
* Java-WebSocket 1.3.0
* Firebase-Admin 4.1.2
* Google json-simple 1.1


## Installation

Use gradle to build the project.
Run `build/libs/ekho-server-*-SNAPSHOT.jar` as standard Java jar file.
Note that there is currently no SSL configuration so Websocket Secure connections rely on SSL by the deployment environment, such as Heroku.


## Deployment Options

* Heroku: The current instance deployed on Heroku successfully connects devices and clients over SSL. The public Heroku domain is useful for the frontend Websocket client. However, if the process is "idle", as in it does not detect activity frequently enough (every 2 minutes), it stops the process. The Websocket server also disconnects after about 1 minute of inactivity. Good for short-term test environment.
* Google Compute Engine: SSL is not natively supported, so some extra work will need to be done to properly configure the connections. Also, the VM's need manual installation and configuration to keep the server running. Must reserve a static IP address and only one VM for frontend Websocket client to connect to.
* Google App Engine: SSL is supported, but there is little control over deployment. May be best long-term solution, but cannot just deploy as `*.jar`. Has public "appspot" domain for the frontend Websocket client.
* Amazon EC2 Instance: **TODO**.


## Implementation

1. On server start, initialize the Firebase connection then start XMPP client and Websocket server - **EkhoEntry**
2. A connection to Firebase is established. - **FirebaseManager**
	* Credentials are in `resources/ekho-beta-firebase-*.json`. Gradle is configured to include this in the `.jar` file, but if the file is not found the server exits.
3. Connect to Firebase CCS as XMPP client. - **XMPPServer/XMPPManager**
3. Start the Websocket server. - **WSServer/SimpleServer**


## Functionality

* Facilitate bidirectional messages between device and client with necessary reformatting of payloads.
* Retrieve and update client and device tokens in Firebase Database.


## What's Next

* Support SSL independently of deployed server?
