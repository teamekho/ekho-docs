# EkhoBot

Ekho's chat bot in the web client.    


## Requirements and Dependencies

* Weather - Dark Sky and Open Weather APIs
* News - NewsAPI
* Quotes - Forismatic API


## Implementation

All messages sent to the EkhoBot channel are handled separately by the server.


## Functionality

* `help` - Displays list of supported commands.
* `weather (city)` - Returns current weather. City name is optional, currently defaults to Baltimore. Future feature to default this to the location of the device.
* `news` - Returns a news article. Future feature to select news category.
* `quote` - Returns a quote and its author.


## What's Next

* NLP parsing of input
* More handlers!
	* Reminders service
