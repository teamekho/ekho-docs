# Ekho App

Ekho's Android application sends and receives XMPP messages with the Firebase server.    


## Requirements and Dependencies

* JDK 1.7
* Gradle 2.2.0
* Android SDK 25
* Firebase 10

## Installation

Build using **gradle**.


## Implementation

1. On app start, user authenticates using Google credentials. - **LoginActivity**, **MainActivity** 
2. A connection to Firebase is established and send the device token to the server. - **FirebaseManager**
3. Device's permissions, contacts, and recent SMS messages are retrieved. - **PermissionChecker**, **ContactManager**, **SmsFetcher**
4. Broadcast receiver is registered to listen for `SMS_RECEIVED`. - **MainActivity**
   * On incoming SMS received by the device, the message is restructured into the payload of type `receive` to be sent to the Firebase Cloud Messaging server.
   * On outgoing SMS to be sent by the device, the message received from Firebase is restructured and sent as an SMS text message. 


## Functionality

* **CONNECT** - Connect to the browser using the `sessionId`.
* **TEST** - Send test `{ Hello: World }` message to the server.
* **SYNC** - Fetch last `syncLimit` number of SMS messages and send to the server.
* **LOGOUT** - Log out user and clear saved credentials.


## What's Next

* Support MMS messages.
* Send notification when client logs in.
* Support location service for EkhoBot requests.
* Run as background service.


## Payload Specifications

### Incoming SMS received by device

This is handled by Android BroadcastReceiver, TelephonyManager, and SmsMessage.    
Note that the device type (CDMA or GSM) affects the PDU format. 


### Incoming message forwarded to server
```javascript
{
	message_id: MESSAGE_COUNTER,
	message_type: "receive",
	data: {
		receive: {
			sender: CONTACT_NAME_OF_SENDER,
			number: PHONE_NUMBER_OF_SENDER,
			message: SMS_MESSAGE_BODY,
			date: SMS_TIMESTAMP,
			direction: "incoming"
		}
	}
}
```
Note that if the contact name is unknown, the PHONE_NUMBER_OF_SENDER is used again.


### Outgoing message from server
```javascript
{
	from: "CLIENT_TOKEN?",
	message_id: MESSAGE_COUNTER,
	message_type: "send",
	data: {
		send: {
			number: PHONE_NUMBER_OF_RECIPIENT,
			message: SMS_MESSAGE_BODY,
		}
	}
}
```

### Outgoing SMS to be sent by device

The SMSManager `sendTextMessage( PHONE_NUMBER_OF_RECIPIENT, scAddress=null, MESSAGE_BODY, sentIntent=null, deliveryIntent=null )` function takes 5 parameters.


### Sample XMPP Message
```xml
<message 
    to='FIREBASE_SENDER_ID@gcm.googleapis.com' 
    from='devices@gcm.googleapis.com' 
    type='normal'>
    <gcm xmlns="google:mobile:data">
        {
            "data": {
                "messageBody": "{\"hello\":\"world\"}",
                "message_type":"test" 
            }, 
            "time_to_live":0,
            "from":"DEVICE_TOKEN",
            "message_id":"0",
            "category":"com.thc.ekho"
        }
    </gcm>
</message>
```

