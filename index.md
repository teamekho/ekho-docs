# Ekho!

Desktop SMS application that is lightweight and secure.


* Ekho App - Android application
* Ekho Server - Java server
* Ekho Front - Web client frontend
* Ekho Bot - Chatbot Assistant


# Usage (as of 5/14/17)

* Ekho App

	* Build and install the app using gradle. The latest `app.apk` file can be [downloaded here](https://firebasestorage.googleapis.com/v0/b/ekho-beta.appspot.com/o/app-debug.apk?alt=media&token=678d17de-06b4-4804-b2bd-90edbde11398).
	* Sign in with your Google account as prompted, and enter the `sessionId` from the browser.

* Ekho Server

	* This is currently deployed on [Heroku here](https://dashboard.heroku.com/apps/whispering-falls-57359), but it frequently goes into "idle" and shuts down. It may need a little time to start up before it can respond to requests from clients or devices.

* Ekho Front

	* This is currently hosted by [Firebase Hosting here](https://ekho-beta.firebaseapp.com).
	* Click "Sign In" and enter the `sessionId` shown in the device application and select "Connect". When connected, the page will update to the chat interface.
	* If the user avatar in the upper right is green, then the Websocket connection is open. If it is red, then the Websocket connection is closed. When the synced messages are loading, a loading icon will display. Messages cannot be sent while messages are syncing.
	* Switching channels in the list on the left also requires double-clicking.
	* If the Websocket connection closes due to inactivity, select "Refresh" in the top menu bar. This will restart the Websocket connection and send a new sync request. The frontend currently sends a "ping" to the server every 30 seconds to keep the connection alive.
	* The search bar can be used to find Google contacts to start new channels.


# Team

* Tiffany Chung
* Gabriel Garcia
