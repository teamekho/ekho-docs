# Ekho Front

Ekho's frontend web client.    


## Requirements and Dependencies

See `package.json` for specific dependencies.

* NPM
* NodeJS
* React
* Redux
* Browserify
* Material Design UI
* Express
* Google OAuth2 API
* Google Cloud (Deployment to Firebase)


## Installation

1. `npm install`
2. `npm run build`
3. `npm run start`
4. Open `localhost:3000` in the browser.


## Deployment to Firebase Hosting

1. Follow online instructions to configure the Firebase CLI.
2. Everything `app.js` needs to run the Express app is in `public/`. This is where Firebase looks for what to deploy.
4. `firebase deploy`
5. Open `https://ekho-beta.firebaseapp.com` in the browser.


Firebase Hosting only supports static pages, so the CLI deploys the whole `public` directory and serves the `index.html` in there.


## Implementation

1. Start the Express server to serve `index.html`. - **app**
2. Render the ReactDOM with the route paths. - **main**
3. When user connects with `sessionID` in the mobile application, sign-in with Google OAuth and retrieve user's contacts with respective phone numbers - **containers/Welcome**, **actions/authActions**
4. Initiate WebsocketSecure connection to server - **containers/ChatContainer**, **actions/actions**
5. When the Websocket is opened, a sync request is sent. It takes a few seconds to populate the messages.
6. Due to potential limitations of the server and the Websocket disconnecting, there is a "Refresh" option in the top menu bar to refresh the Websocket connection and send another sync request. For now, the frontend sends a "ping" every 30 seconds to keep the Websocket connection alive. This is a temporary fix.
7. All messages to the "EkhoBot" channel are handled differently by the server and do not send SMS messages; they instead return appropriate responses.


## Functionality

* Send and receive SMS text messages to and from device.
* Create new conversations from Google Contacts.
* EkhoBot!


## What's Next

* Electron desktop client.
* Tighter variable type requirements and error handling.


## Specifications


### Payload Specifications


#### Outgoing message to server
```javascript
{
	message_type: "send" or "bot",
	message_id: MESSAGE_COUNTER,
	data: {
		id: MESSAGE_COUNTER,
		sender: CONTACT_NAME_OF_SENDER,
		number: PHONE_NUMBER_OF_RECIPIENT,
		message: MESSAGE_BODY,
		status: 0,
		timestamp: "now";
		direction: "outgoing"
	},
	self: USER_ID
}
```
Note that new channels can currently only be created from existing Google Contacts, and that the timestamp is currently unused.


#### Incoming message from server
```javascript
{
	message_type: "receive",
	data: {
		sender: CONTACT_NAME_OF_SENDER,
		number: PHONE_NUMBER_OF_SENDER,
		message: SMS_MESSAGE_BODY,
		date: SMS_TIMESTAMP,
		direction: "incoming" or "outgoing"
	}
}
```
Note that if a message is sent from the device, the message is currently not forwarded to the client. Also, the "sync" messages have an extra layer in the "data" filed to denote that it is a "synced" message. It may be beneficial later on to enforce this type for all messages.


#### Incoming messages from server on sync
```javascript
{
	message_type: "sync",
	data: {
		synced: {
			messages: [
				{
					sender: CONTACT_NAME_OF_SENDER,
					number: PHONE_NUMBER_OF_SENDER,
					message: SMS_MESSAGE_BODY,
					date: SMS_TIMESTAMP,
					direction: "incoming" or "outgoing"
				}
				...
			]
		}
	}
}
```


#### Incoming messages from EkhoBot
```javascript
{
	data: {
		bot: RESPONSE_BODY (String)
	}
}
```



### Class Definitions

See these in `components/`.

#### Channel

Channels are conversation threads.

```javascript
{
	id: CHANNEL_ID, (int)
	nickname: CHANNEL_NAME, (String)
	contacts: [ LIST_OF_CONTACTS ], (Array of Contacts)
	messageList: [ LIST_OF_MESSAGES ], (Array of Messages)
	unreadCount: NUMBER_OF_UNREAD_MESSAGES (int)
}
```
CHANNEL_ID is derived from the number of channels. "0" is reserved for EkhoBot.


#### Contact

Contacts only contain information about the contact and *not* conversation threads.    
Each phone number is a unique Contact, so it is possible for many Contacts to have the same name but different number. The `displayName` specifies the type of phone number , ex. "Tiffany Chung (mobile)".

```javascript
{
	id: CONTACT_PHONE_NUMBER, (String)
	name: CONTACT_FULL_NAME, (String)
	number: CONTACT_PHONE_NUMBER, (String)
	displayName: CONTACT_NAME_AND_TYPE_OF_NUMBER, (String)
	color: RANDOMLY_GENERATED_COLOR (String in hex format)
}
```


#### Message

```javascript
{
	id: MESSAGE_ID, (int)
	sender: NAME_OF_SENDER, (String)
	number: PHONE_NUMBER_OF_SENDER, (String)
	message: MESSAGE_BODY, (String)
	status: 0_IF_UNREAD_AND_1_IF_READ, (int)
	timestamp: TIMESTAMP, (?)
	direction: INCOMING_OR_OUTGOING (String)
}
```


